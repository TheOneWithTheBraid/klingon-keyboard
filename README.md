# Klingon xkb keyboard layout

The Klingon quja' keyboard layout allows you to easily type Klingon on your computer.

* Support for `tlhIngan Hol` input
* Support for `xifan Hol` input
* Support for ` ` (Unicode) input according to [L2/16-329](https://unicode.org/L2/L2016/16329-piqad-returns.pdf)
* Easily type `DIvI Holmey` (human languages) using all kinds of Latin accents
* Following the famous `quja'` layout by ASK Klingon

![Klingon Keyboard](screenshot/keyboard-patched.svg)

In the [fonts/](fonts/) folder you can find some information and resources on Klingon fonts.

## Setup

### Dependencies

Please install:
```
ydotool xbindkeys xorg
```

### Integrate layout

```shell
sudo cp tlh /usr/share/X11/xkb/symbols/tlh
```

Now edit `/usr/share/X11/xkb/rules/base.xml` and `/usr/share/X11/xkb/rules/evdev.xml` each:

Look for the Afghani layout:
```xml
    <layout>
      <configItem>
        <name>af</name>
        <!-- Keyboard indicator for Afghani layouts -->
```

and insert the following **directly before that section**:

```xml
    <layout>
      <configItem>
        <name>tlh</name>
        <!-- Keyboard indicator for Klingon layouts -->
        <shortDescription>tlh</shortDescription>
        <description>tlhIngan Hol</description>
        <languageList>
          <iso639Id>tlh</iso639Id>
        </languageList>
      </configItem>
      <variantList>
        <variant>
          <configItem>
            <name>tlh</name>
            <description>tlhIngan Hol tlh' DIvI Hol je'</description>
          </configItem>
        </variant>
      </variantList>
    </layout>
```

### Bind `xIfan Hol` to `tlhIngan Hol`

Now you should already have a basic Klingon keyboard layout available, but letters like `tlh`, `gh`, `ch` and `ng` won't work.

```shell
xbindkeys --defaults > $HOME/.xbindkeysrc
```

Open `$HOME/.xbindkeysrc` and **comment every exiting binding**. Afterward add the Klingon bindings:

```
"ydotool type tlh"
    m:0x2000 + c:55
    f

"ydotool type ch"
    m:0x0 + c:29
    y

"ydotool type ng"
    m:0x0 + c:46
    l

"ydotool type gh"
    m:0x0 + c:32
    o
```

now you enable xbindkeys by

```shell
echo "xbindkeys" | tee -a $HOME/.profile
```

### Yet you are not allowed to send keys

Allow yourself to access `/dev/uinput`:

```shell
sudo groupadd uinput
sudo usermod -aG uinput $USER
echo "KERNEL==\"uinput\", GROUP=\"uinput\", MODE=\"0660\", OPTIONS+=\"static_node=uinput\"" | sudo tee /etc/udev/rules.d/80-uinput.rules > /dev/null
```

### Reboot

Reboot and enjoy the world's most confusing keyboard layout!

```shell
sudo reboot
```

## Notable resources

* [boQwI'](https://github.com/De7vID/klingon-assistant)
* https://dadap.github.io/pIqaD-tools/universal-transliterator/?xifan=qaplaz
* [ASK Klingon](https://play.google.com/store/apps/details?id=com.anysoftkeyboard.languagepack.tlhIngan)

# License

As long as not marked differently, this work is WTFPL-licensed. For more information see [LICENSE](LICENSE).