# Klingon fonts

When using the Klgingon keyboard layout it is recommended to install Klingon fonts.

Here is a list of Klingon fonts:

| Font                 | writing   | license | face             |
| :------------------- | :-------- | :-----: | :--------------- |
| Klingon pIqaD HaSta  | unicode   |   OFL   | block            |
| Klingon pIqaD Mandel | unicode   |   OFL   | block            |
| Klingon pIqaD vaHbo' | unicode   |   OFL   | handwriting      |
| pIqaD qolqoS         | unicode   |   OFL   | light, hand-like |
| KApIqaD              | xifan Hol |    ?    | block            |
| Code2000             | unicode   |    ?    | block            |